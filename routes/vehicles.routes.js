const express = require('express')
const Vehicle = require('../models/Vehicle')
const router = express.Router()

router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params

    const deleted = await Vehicle.findByIdAndDelete(id)

    if (deleted) {
      return res.status(200).json('Vehicle deleted')
    } else {
      return res.status(200).json('Vehicle not found')
    }
  } catch (error) {
    next(new Error(error))
  }
})

router.put('/add-vehicle', async (req, res, next) => {
  try {
    const { vehicleId, carId } = req.body

    const updatedVehicle = await Vehicle.findByIdAndUpdate(
      vehicleId,
      {
        $push: { cars: carId },
      },
      { new: true },
    )

    return res.json(updatedVehicle)
  } catch (error) {
    next(error.message)
  }
})
router.post('/create', async (req, res, next) => {
  try {
    const { type, wheels } = req.body
    const newVehicle = await new Vehicle({
      type,
      wheels,
    })
    const createdVehicle = await newVehicle.save()
    return res.status(200).json(createdVehicle)
  } catch (error) {
    next(new Error(error))
  }
})

router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params

    const vehicle = await Vehicle.findById(id).populate('cars')

    return res.json(vehicle)
  } catch (error) {
    next(error)
  }
})

module.exports = router
