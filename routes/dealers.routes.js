const express = require('express')
const Dealer = require('../models/Dealer')
const router = express.Router()

router.get('/', async (req, res, next) => {
  try {
    const dealers = await Dealer.find()
    return res.status(200).json(dealers)
  } catch (error) {
    next(new Error(error))
  }
})

router.put('/add-car', async (req, res, next) => {
  try {
    const { dealerId, carId } = req.body

    const updatedDealer = await Dealer.findByIdAndUpdate(
      dealerId,
      {
        $push: { cars: carId },
      },
      { new: true },
    )

    return res.json(updatedDealer)
  } catch (error) {
    next(error.message)
  }
})
router.post('/create', async (req, res, next) => {
  try {
    const { name, location } = req.body
    const newDealer = await new Dealer({
      name,
      location,
    })
    const createdDealer = await newDealer.save()
    return res.status(200).json(createdDealer)
  } catch (error) {
    next(new Error(error))
  }
})

router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params

    const dealer = await Dealer.findById(id).populate('cars')

    return res.json(dealer)
  } catch (error) {
    next(error)
  }
})

module.exports = router
