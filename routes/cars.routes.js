const express = require('express')
const Car = require('../models/Car')
const router = express.Router()

router.get('/', async (req, res, next) => {
  try {
    const cars = await Car.find()
    return res.json(cars)
  } catch (error) {
    console.log(error)
    next(new Error(error))
  }
})

router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const car = await Car.findById(id)
    if (car) {
      return res.json(car)
    } else {
      return res.status(404).json('Car not found')
    }
  } catch (error) {
    next(new Error(error))
  }
})

router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const deleted = await Car.findByIdAndDelete(id)

    if (deleted) {
      return res.status(200).json('Car deleted')
    }
    return res.status(200).json('Car not found')
  } catch (error) {
    next(new Error(error))
  }
})

router.put('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const update = req.body

    const updatedCar = await Car.findByIdAndUpdate(id, update, { new: true })

    if (updatedCar) {
      return res.status(200).json('Car updated')
    }
  } catch (error) {
    next(new Error(error))
  }
})

router.get('/brands/:brand', async (req, res, next) => {
  try {
    const { brand } = req.params
    const carByBrand = await Car.find({ brand: brand })
    if (carByBrand) {
      return res.json(carByBrand)
    } else {
      return res.status(404).json('Brand not found')
    }
  } catch (error) {
    next(new Error(error))
  }
})
router.get('/years/:year', async (req, res, next) => {
  try {
    const { year } = req.params
    const carsByYear = await Car.find({ year: { $gte: year } })
    if (carsByYear) {
      return res.json(carsByYear)
    } else {
      return res.status(500).json(error)
    }
  } catch (error) {
    next(new Error(error))
  }
})

router.post('/create', async (req, res, next) => {
  try {
    const { brand, model, type, color, year } = req.body
    const newCar = await new Car({ brand, model, type, color, year })

    const createdCar = await newCar.save()

    return res.status(201).json(createdCar)
  } catch (error) {
    next(new Error(error))
  }
})

module.exports = router
