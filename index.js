const express = require('express')
const db = require('./db')

const carsRoutes = require('./routes/cars.routes')

db.connect()

const PORT = 3000

const server = express()

server.use(express.json())
server.use(express.urlencoded({ extended: true }))

const router = express.Router()

server.use('/cars', carsRoutes)

server.use('*', (req, res, next) => {
  const error = new Error('Route not found')
  error.status = 404
  next(new Error(error))
})

server.use((error, req, res, next) => {
  return res
    .status(error.status || 500)
    .json(error.message || 'Unexpected error')
})

server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`)
})
