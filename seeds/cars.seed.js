const mongoose = require('mongoose')
const db = require('../db.js')
const Car = require('../models/Car')

const cars = [
  {
    brand: 'Peugeot',
    model: '5008',
    type: 'SUV',
    color: 'White',
    year: 2020,
  },
  {
    brand: 'KIA',
    model: 'Ceed',
    type: 'SUV',
    color: 'Blue',
    year: 2018,
  },
  {
    brand: 'Seat',
    model: 'Leon',
    type: 'Compact',
    color: 'Red',
    year: 2019,
  },
  {
    brand: 'Audi',
    model: 'A6',
    type: 'Sedan',
    color: 'Blue',
    year: 2019,
  },
]

mongoose
  .connect(db.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allCars = await Car.find()

    if (allCars.length) {
      console.log('Deleting all cars')
      await Car.collection.drop()
    }
  })
  .catch((error) => {
    console.log('Error deleting data:', error)
  })
  .then(async () => {
    await Car.insertMany(cars)
    console.log('Data inserted correctly')
  })
  .catch((error) => {
    console.log('Error creating data:', error)
  })
  .finally(() => mongoose.disconnect())
