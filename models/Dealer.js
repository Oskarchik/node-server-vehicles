const mongoose = require('mongoose')

const Schema = mongoose.Schema

const dealerSchema = new Schema(
  {
    name: { type: String },
    location: { type: String },
    cars: [{ type: mongoose.Types.ObjectId, ref: 'Car' }],
  },
  {
    timestamps: true,
  },
)

const Dealer = mongoose.model('Dealer', dealerSchema)

module.exports = Dealer
