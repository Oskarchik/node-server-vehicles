const mongoose = require('mongoose')

const Schema = mongoose.Schema

const vehicleSchema = new Schema(
  {
    type: { type: String, required: true },
    wheels: { type: Number, required: true },
    cars: [{ type: mongoose.Types.ObjectId, ref: 'Car' }],
  },
  {
    timestamps: true,
  },
)

const Vehicle = mongoose.model('Vehicle', vehicleSchema)

module.exports = Vehicle
